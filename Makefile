B=_build
all:	
	cmake -B $(B) -S .
	cmake --build $(B) -- VERBOSE=1
	cd $(B) && ctest -V
